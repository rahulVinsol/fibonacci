enum FibonacciError: Error {
	case invalidInput
	case overflow
}

func getFibonacciError(n: Int) throws -> Int {
	switch n { 
		case let n where n > 92: throw FibonacciError.overflow
		case let n where n <= 0: throw FibonacciError.invalidInput
		default: 
			var n = n
	
			var first = 1
			var second = 1
			var third = 1
	
			while n>2 {
				third = first + second
				first = second
				second = third
				n-=1
			}
			return third
	}
}
